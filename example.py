#__________________________________________________________________________________
#
#	FILENAME: example.py
#	CREATED: 12/02/17
#	AUTHOR: Benjamin Ehrlich
#	PURPOSE: An example of how to use SAP. 
#
#
#

from sap import *

#Our training datasapTrain('I love this sandwich.', 'pos')sapTrain('this is an amazing place!', 'pos')sapTrain('I feel very good about these beers.', 'pos')sapTrain('this is my best work.', 'pos')sapTrain('what an awesome view', 'pos')sapTrain('I do not like this restaurant', 'neg')sapTrain('I am tired of this stuff.', 'neg')sapTrain('I can\'t deal with this', 'neg')sapTrain('he is my sworn enemy!', 'neg')sapTrain('my boss is horrible.', 'neg')
#Should print a negative numberprint(sapEvaluate("Don't get me wrong. I love my boss, but he is horrible at communicating. He's one of my best friends and I love having a beer with him after work, but I can't deal with his micro managing."))

#should print a positive numberprint(sapEvaluate("My co-worker Janet on the other hand is a great person. When I'm tired she is always there to pick up the slack. She's an awesome asset to the team."))

#Should print 0
print(sapEvaluate(""))