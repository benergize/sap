#__________________________________________________________________________________
#
#	FILENAME: sap.py
#	CREATED: 12/02/17
#	AUTHOR: Benjamin Ehrlich
#	PURPOSE: Functions for the training and evaluation of sentiments. A very
#	(cont.) simple sentiment analysis tool.
#
#
#


#-
positiveAverage = 0
negativeAverage = 0

#Lists will be populated with training data
positiveList = []
negativeList = []
	
#Function to train the evaluator what is positive and what is negative
def sapTrain(input,pn):

	#Globalize necesary variables
	global negativeList
	global positiveList
	global positiveAverage
	global negativeAverage
	
	#Split the supplied sentence into a list of words
	wordList = input.split(" ")
	
	#Iterate through the list of words
	for v in range(len(wordList)):
	
		#If the user has specified that this is a positive sentence, add the word to the positive list
		if pn == "pos":
			positiveList.append(wordList[v])
			positiveAverage += len(wordList[v])
			positiveAverage /= 2
			
		#If the user has specified that this is a negative sentence, add the word to the negative list
		if pn == "neg":
			negativeList.append(wordList[v])
			negativeAverage += len(wordList[v])
			negativeAverage /= 2

#Evaluate a sentence based on the trained data
def sapEvaluate(input):

	#Globalize necesary variables
	global negativeList
	global positiveList
	global positiveAverage
	global negativeAverage
	
	#The score variable being above or below 0 will determine whether the sentence is positive or negative
	score = 0
	
	#Split the supplied sentence into a list of words
	wordList = input.split(" ")
	
	#Iterate through the list of words
	for v in range(len(wordList)):
	
		#Iterate through the list of negative words
		for x in range(len(negativeList)):
		
			#If the current element in the wordList is in the negativeList
			if wordList[v] == negativeList[x]:
			
				#Decrease the score
				score -= 1
				
		#Iterate through the list of positive words
		for y in range(len(positiveList)):
			
			#If the current element in the wordList is in the positiveList
			if wordList[v] == positiveList[y]:
			
				#Increase the score
				score += 1
	
	#Return the score. If it's above 0 it's positive, if it's below 0 it's negative.
	return score
	
